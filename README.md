# Social Media Giveaways

Development of customer facing landing page for Social Media Giveaways with automated email opt-in process. 
Development of administration page for Social Media team to input giveaway content and variably display content on customer facing page.

Process Details
------------------

Social Media team visits Admin Content Landing Page; enters unique giveaway code and any content they want to appear on the giveaway form.
After Submission, all data is inserted into a Content Data Extension within SFMC where it can be pulled into the giveaway page via AMPscript.
Landing page content is generated on the customer giveaway page using the unique giveaway code as a query perameter which is used as the key to identify what content to lookup.
Once a user enters the giveaway, their info is inserted into a master giveaway data extension which is then filtered for optins and sends them into the established Welcome Journey.

-- Other Considerations --
Users can only enter each giveaway once; primary key set to Email-Giveaway Code combo (allows entry to future giveaways)
Customer Landing Page content changes based on following situations:
- Giveaway has not started
- Giveaway has ended
- They have already entered that giveaway

-- Additonal Admin Functionality --
- Ability to generate content for existing giveaway to update content
- Random winner generator
    - Based on looking up all rows with entered giveaway code, capturing number of rows and setting that as max number for random number generation, 
        assigning row lookup based on random number selected.
- Pull following information for any existing giveaway:
    Giveaway Code, Total Entries, Total New Subscribers


